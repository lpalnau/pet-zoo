import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StyleYourPetComponent } from './style-your-pet.component';

describe('StyleYourPetComponent', () => {
  let component: StyleYourPetComponent;
  let fixture: ComponentFixture<StyleYourPetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StyleYourPetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StyleYourPetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
