import { Component, OnInit } from '@angular/core';
import {PetService} from '../pet.service';
import {NavService} from '../nav.service';

@Component({
  selector: 'app-style-your-pet',
  templateUrl: './style-your-pet.component.html',
  styleUrls: ['./style-your-pet.component.css']
})
export class StyleYourPetComponent implements OnInit {

  constructor(public petService: PetService, public navService: NavService) {
    navService.screenName = 'Dress Up Your Pet';
    navService.previousScreenUrlPath = '/name-your-pet';
    navService.nextScreenUrlPath = '/feed-your-pet';
    navService.showStartOverButton = true;
    navService.headerIcon = null;
  }

  ngOnInit() {
  }

  addStyleIfNotPresentOrRemoveStyleIfPresent(dress: string) {
    const foundAtIndex = this.petService.pet.styles.indexOf(dress);
    console.log(`found ${dress} at index ${foundAtIndex}`);
    if (foundAtIndex === -1) {
      this.petService.pet.styles.push(dress);
    } else {
      this.petService.pet.styles.splice(foundAtIndex, 1);
    }
  }

  addStyle(dress: string) {
    const foundAtIndex = this.petService.pet.styles.indexOf(dress);
    console.log(`found ${dress} at index ${foundAtIndex}`);
    if (foundAtIndex === -1) {
      this.petService.pet.styles.push(dress);
    }
  }

  removeStyle(dress: string) {
    const foundAtIndex = this.petService.pet.styles.indexOf(dress);
    console.log(`found ${dress} at index ${foundAtIndex}`);
    if (foundAtIndex === -1) {
      return;
    } else {
      this.petService.pet.styles.splice(foundAtIndex, 1);
    }
  }

  changeFootwear(footwear: string) {
    if (footwear === 'shoes') {
      this.removeStyle('roller skates');
    } else {
      this.removeStyle('shoes');
    }

    this.addStyle(footwear);
  }
}
