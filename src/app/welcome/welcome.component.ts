import { Component, OnInit } from '@angular/core';
import {NavService} from '../nav.service';
import {PetService} from '../pet.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  constructor(public navService: NavService, public petService: PetService) {
    navService.showStartOverButton = false;
    navService.previousScreenUrlPath = null;
    navService.nextScreenUrlPath = null;
    navService.headerIcon = null;
  }

  ngOnInit() {
    this.petService.pet.foods = [];
    this.petService.pet.styles = [];
    this.petService.pet.picture = null;
    this.petService.pet.name = null;
  }

}
