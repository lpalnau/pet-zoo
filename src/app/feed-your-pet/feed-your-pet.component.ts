import { Component, OnInit } from '@angular/core';
import {NavService} from '../nav.service';
import {PetService} from '../pet.service';
import {Food} from '../food';

@Component({
  selector: 'app-feed-your-pet',
  templateUrl: './feed-your-pet.component.html',
  styleUrls: ['./feed-your-pet.component.css']
})
export class FeedYourPetComponent implements OnInit {
  public foods: Food[] = [];

  constructor(public petService: PetService, public navService: NavService) {
    navService.screenName = 'Feed Your Pet';
    navService.previousScreenUrlPath = '/style-your-pet';
    navService.nextScreenUrlPath = '/good-job';
    navService.showStartOverButton = true;
    navService.headerIcon = null;

    this.foods.push(new Food('assets/bone.gif', 'bone'));
    this.foods.push(new Food('assets/popcorn.jpg', 'popcorn'));
    this.foods.push(new Food('assets/cotton-candy.jpg', 'cotton candy'));
    this.foods.push(new Food('assets/cake.jpg', 'cake'));
    this.foods.push(new Food('assets/ice-cream.jpg', 'ice cream'));
  }

  ngOnInit() {
  }

  public feed(food: string) {
    this.petService.pet.foods.push(food);
  }

}
