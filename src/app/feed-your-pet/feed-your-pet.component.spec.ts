import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeedYourPetComponent } from './feed-your-pet.component';

describe('FeedYourPetComponent', () => {
  let component: FeedYourPetComponent;
  let fixture: ComponentFixture<FeedYourPetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeedYourPetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeedYourPetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
