export class Pet {
  public animalFamily: string;
  public name: string;
  public picture: string;
  public description: string;
  public styles: string[] = [];
  public foods: string[] = [];
  public imageXml: string;

  constructor() {}
}
