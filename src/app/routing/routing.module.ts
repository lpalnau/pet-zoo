import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {WelcomeComponent} from '../welcome/welcome.component';
import {ChooseYourPetComponent} from '../choose-your-pet/choose-your-pet.component';
import {StyleYourPetComponent} from "../style-your-pet/style-your-pet.component";
import {NameYourPetComponent} from "../name-your-pet/name-your-pet.component";
import {FeedYourPetComponent} from "../feed-your-pet/feed-your-pet.component";
import {GoodJobComponent} from "../good-job/good-job.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: '/welcome',
    pathMatch: 'full'
  },
  {
    path: 'welcome',
    component: WelcomeComponent
  },
  {
    path: 'choose-your-pet',
    component: ChooseYourPetComponent
  },
  {
    path: 'name-your-pet',
    component: NameYourPetComponent
  },
  {
    path: 'style-your-pet',
    component: StyleYourPetComponent
  },
  {
    path: 'feed-your-pet',
    component: FeedYourPetComponent
  },
  {
    path: 'good-job',
    component: GoodJobComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule ],
  declarations: [ ]
})
export class RoutingModule { }
