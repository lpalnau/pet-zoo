import { Component, OnInit } from '@angular/core';
import {NavService} from '../nav.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  public navService: NavService;

  constructor(navService: NavService) {
    this.navService = navService;
  }

  ngOnInit() {
  }

}
