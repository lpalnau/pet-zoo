import { Component, OnInit } from '@angular/core';
import {Pet} from '../pet';
import {PetService} from '../pet.service';
import {NavService} from '../nav.service';

@Component({
  selector: 'app-choose-your-pet',
  templateUrl: './choose-your-pet.component.html',
  styleUrls: ['./choose-your-pet.component.css']
})
export class ChooseYourPetComponent implements OnInit {
  public pets: Pet[] = [];

  constructor(public petService: PetService, public navService: NavService) {
    navService.screenName = 'Choose Your Pet';
    navService.showStartOverButton = false;
    navService.previousScreenUrlPath = '/welcome';
    navService.nextScreenUrlPath = null; // its actually '/name-your-pet', but the choose buttons do this in place of the footer nav
    navService.headerIcon = null;

    this.pets.push(petService.createPet('Giraffe', 'assets/giraffe.svg', 'Giraffes are great pets', ''));
    this.pets.push(petService.createPet('Unicorn', 'assets/unicorn.svg', 'Unicorns are majestic pets', ''));
    this.pets.push(petService.createPet('Cheetah', 'assets/cheetah.svg', 'Cheetahs are fast', ''));
    this.pets.push(petService.createPet('Otter', 'assets/otter.svg', 'Otters love to swim', ''));
    this.pets.push(petService.createPet('Owl', 'assets/owl.svg', 'Owl goes hoo hoo', ''));
    this.pets.push(petService.createPet('Penguin', 'assets/penguin.svg', 'Penguins are the birds of the sea', ''));
  }

  choose(pet: Pet) {
    this.petService.pet.animalFamily = pet.animalFamily;
    this.petService.pet.picture = pet.picture;
  }


  ngOnInit() {
  }
}
