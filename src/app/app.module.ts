import { BrowserModule } from '@angular/platform-browser';
import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { FormsModule } from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MdButtonModule, MdCheckboxModule, MdInputModule, MdButtonToggleModule,
        MdIconModule, MdCardModule, MdMenuModule, MdGridListModule, MdToolbarModule} from '@angular/material';

import 'hammerjs';
import { AppComponent } from './app.component';
import { RoutingModule} from './routing/routing.module';
import { HeaderComponent } from './header/header.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { FooterComponent } from './footer/footer.component';
import { ChooseYourPetComponent } from './choose-your-pet/choose-your-pet.component';
import { NameYourPetComponent } from './name-your-pet/name-your-pet.component';
import { StyleYourPetComponent } from './style-your-pet/style-your-pet.component';
import { FeedYourPetComponent } from './feed-your-pet/feed-your-pet.component';
import { GoodJobComponent } from './good-job/good-job.component';
import { DisplayPetComponent } from './display-pet/display-pet.component';
import { PetService } from './pet.service';
import {NavService} from './nav.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    WelcomeComponent,
    FooterComponent,
    ChooseYourPetComponent,
    NameYourPetComponent,
    StyleYourPetComponent,
    FeedYourPetComponent,
    GoodJobComponent,
    DisplayPetComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MdButtonModule,
    MdCheckboxModule,
    MdInputModule,
    MdButtonToggleModule,
    MdIconModule,
    MdCardModule,
    MdMenuModule,
    MdGridListModule,
    MdToolbarModule,
    RoutingModule
  ],
  providers: [PetService, NavService],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule { }
