import { Injectable } from '@angular/core';

@Injectable()
export class NavService {
  public screenName: string;
  public previousScreenUrlPath: string;
  public nextScreenUrlPath: string;
  public showStartOverButton: boolean;
  public headerIcon: string;
  constructor() { }

}
