import {Component, HostBinding, Input, OnInit} from '@angular/core';
import {PetService} from '../pet.service';

@Component({
  selector: 'app-display-pet',
  templateUrl: './display-pet.component.html',
  styleUrls: ['./display-pet.component.css']
})
export class DisplayPetComponent implements OnInit {
  @Input()
  public nameEditable: boolean;

  petService: PetService;
  constructor(petService: PetService) {
    this.petService = petService;
  }

  ngOnInit() {
  }

}
