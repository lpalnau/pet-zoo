import { Injectable } from '@angular/core';
import {Pet} from './pet';

@Injectable()
export class PetService {
  public pet: Pet = new Pet();
  public otterImageXml: String = '';

  constructor() {
    //http.get('assets/otter.svg').
  }

  public createPet(animalFamily: string, picture: string, description: string, imageXml: string): Pet {
    const newPet = new Pet();
    newPet.animalFamily = animalFamily;
    newPet.picture = picture;
    newPet.description = description;
    newPet.imageXml = imageXml;
    return newPet;
  }
}
