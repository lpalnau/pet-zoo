import { Component, OnInit } from '@angular/core';
import {NavService} from '../nav.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public navService: NavService;

  constructor(navService: NavService) {
    this.navService = navService;
  }

  ngOnInit() {
  }

}
