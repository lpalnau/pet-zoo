import { Component, OnInit } from '@angular/core';
import {NavService} from '../nav.service';

@Component({
  selector: 'app-good-job',
  templateUrl: './good-job.component.html',
  styleUrls: ['./good-job.component.css']
})
export class GoodJobComponent implements OnInit {

  constructor(public navService: NavService) {
    navService.screenName = 'Good Job';
    navService.previousScreenUrlPath = '/feed-your-pet';
    navService.nextScreenUrlPath = null;
    navService.showStartOverButton = true;
    navService.headerIcon = 'thumb_up';
  }

  ngOnInit() {
  }

}
