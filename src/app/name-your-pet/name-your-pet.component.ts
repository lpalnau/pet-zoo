import { Component, OnInit } from '@angular/core';
import {PetService} from '../pet.service';
import {NavService} from '../nav.service';

@Component({
  selector: 'app-name-your-pet',
  templateUrl: './name-your-pet.component.html',
  styleUrls: ['./name-your-pet.component.css']
})
export class NameYourPetComponent implements OnInit {

  constructor(public petService: PetService, public navService: NavService) {
    navService.screenName = 'Name Your Pet';
    navService.previousScreenUrlPath = '/choose-your-pet';
    navService.nextScreenUrlPath = '/style-your-pet';
    navService.showStartOverButton = true;
    navService.headerIcon = null;
  }

  ngOnInit() {
  }

}
