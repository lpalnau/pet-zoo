import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NameYourPetComponent } from './name-your-pet.component';

describe('NameYourPetComponent', () => {
  let component: NameYourPetComponent;
  let fixture: ComponentFixture<NameYourPetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NameYourPetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NameYourPetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
